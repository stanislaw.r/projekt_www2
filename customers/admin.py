from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from customers.models import User
from items.models import Item


@admin.register(User)
class CustomUserAdmin(admin.ModelAdmin):
    list_display = ("username", "role", "is_superuser")
    list_filter = ("username",)
    search_fields = ["username"]

@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ("name",)
    list_filter = ("name",)
    search_fields = ["name"]


