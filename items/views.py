from django.shortcuts import render, HttpResponseRedirect
from django.urls import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from items.models import Item


def item_create(request):
    if not request.user.is_authenticated:
        raise PermissionDenied()
    if request.user.role == 3:
        raise PermissionDenied()
    data = {}
    if request.method == 'POST':
        data['name'] = request.POST.get('name')
        data['description'] = request.POST.get('description')
        data['price'] = request.POST.get('price')
        data['code'] = request.POST.get('code')
        data['is_available'] = request.POST.get('is_available')
        if data['is_available'] is None:
            data['is_available'] = False
        else:
            data['is_available'] = True
        item = Item(**data)
        if data['name']:
            item.save()
            home = reverse_lazy('home')
            return HttpResponseRedirect(home)

        else:
            data['errors'] = 'Product name required'

    return render(request, 'item_create.html', context=data)


def items_table(request):
    items = Item.objects.all()
    context = {
        'items': items,
    }
    return render(request, 'items_table.html', context=context)


def add_to_cart(request, item_id):
    cart_in_session = request.session['cart'] if 'cart' in request.session else []
    cart_in_session.append(item_id)
    request.session.update({'cart': cart_in_session})
    return JsonResponse({'message': 'Added item to cart.'})


def clear_cart(request):
    request.session.update({'cart': []})
    return JsonResponse({'message': 'Cleared cart.'})


def view_cart(request):
    raw_ids = request.session['cart'] if 'cart' in request.session else []
    unique_ids = {}
    for i in raw_ids:
        item = Item.objects.get(pk=i)
        if item not in unique_ids:
            print(item)
            unique_ids[item] = raw_ids.count(i)

    cost = 0

    for i in unique_ids:
        cost += i.price*unique_ids[i]

    context = {'ids': unique_ids, 'cost': cost}

    print(context)
    return render(request, 'cart_page.html', context=context)
