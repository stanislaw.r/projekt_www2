const clearCart = document.querySelector(".ClearCart")

function handleSubmit(form) {
    form.addEventListener("submit", e => {
        e.preventDefault();
        fetch(`/clear_cart/`, {
            method: 'GET',
        })
            .then(response => response.json())
            .then(data => {
                form.reset();
                alert(data['message'])
            })
            .catch((error) => {
                console.error('Error:', error);
            });

    })
}

handleSubmit(clearCart)